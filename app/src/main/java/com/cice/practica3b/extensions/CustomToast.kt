package com.cice.practica3b.extensions

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.LayoutRes
import com.cice.practica3b.R
import kotlinx.android.synthetic.main.toast_layout.view.*


fun customToast(message: String, context: Activity, imageSrc: Int, position: Int) {

    val toast = Toast(context)
    val linearLayout = LinearLayout(context)

    toast.apply {

        val layout = linearLayout.inflate(R.layout.toast_layout)

        layout.toast_textView.text = message
        layout.toast_image.setBackgroundResource(imageSrc)
        setGravity(position, 0, 0)
        duration = Toast.LENGTH_SHORT
        view = layout
        show()
    }
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}