package com.cice.practica3b.extensions

import android.util.Log
import com.cice.practica3b.BuildConfig


fun String.log(tag: String = "CICE") {

    if (BuildConfig.DEBUG)
        Log.d(tag, this)
}