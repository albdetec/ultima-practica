package com.cice.practica3b.model


data class Weather(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String,
    var dt: Int
)