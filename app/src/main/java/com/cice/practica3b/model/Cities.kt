package com.cice.practica3b.model

data class Cities(
    val coord: Coord,
    val country: String,
    val id: Int,
    val name: String
)