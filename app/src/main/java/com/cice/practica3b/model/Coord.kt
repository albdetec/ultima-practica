package com.cice.practica3b.model

data class Coord(
    val lat: Double,
    val lon: Double
)