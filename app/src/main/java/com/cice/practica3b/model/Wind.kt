package com.cice.practica3b.model


data class Wind(
    val deg: Int,
    val speed: Double
)