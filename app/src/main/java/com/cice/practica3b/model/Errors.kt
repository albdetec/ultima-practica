package com.cice.practica3b.model

data class Errors(
    val cod: String,
    val message: String
)