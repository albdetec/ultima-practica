package com.cice.practica3b.utils

import android.view.View
import android.widget.ImageView
import com.cice.practica3b.R
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.cice.practica3b.usescase.main.globals.*
import java.text.DateFormat.getDateTimeInstance
import java.util.*

@BindingAdapter("setIcon")
fun setIcon(view: ImageView, iconcode: String?) {

    val finalUrl = "https://openweathermap.org/img/w/" + iconcode + ".png"

    Glide
        .with(view.context)
        .load(finalUrl)
        .into(view)
}

@BindingAdapter("weatherBackground")
fun weatherBackground(view: View, weatherType: String?) {

    if (weatherType != null)

        view.background = when {

            (weatherType.toLowerCase().contains(CLEAR))
            -> {
                ContextCompat.getDrawable(view.context, R.drawable.despejado)
            }

            (weatherType.toLowerCase().contains(CLOUDY))
            -> {
                ContextCompat.getDrawable(view.context, R.drawable.parcialmente_nublado)
            }

            (weatherType.toLowerCase().contains(SUNNY))
            -> {
                ContextCompat.getDrawable(view.context, R.drawable.soleado)
            }

            (weatherType.toLowerCase().contains(RAIN)) -> {
                ContextCompat.getDrawable(view.context, R.drawable.lluvia)
            }

            else -> ContextCompat.getDrawable(view.context, R.drawable.despejado)
        }
}

@BindingAdapter("dateUnix")
fun dateUnix(view: TextView, date: Long) {

    val dF = getDateTimeInstance()
    val datex = Date(date * 1000)

    view.text = dF.format(datex).toString()
}

@BindingAdapter("tempText")
fun tempText(view: TextView, temp: Double) {

    when {

        (Locale.getDefault() == localeES) -> {

            if (temp.toString().contains(".")) {

                val tempComa = temp.toString().replace('.', ',')

                view.text = tempComa + "ºC"
            }
        }
        else -> view.text = temp.toString() + "ºF"
    }
}

