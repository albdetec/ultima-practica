package com.cice.practica3b.usescase.main.globals

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.cice.practica3b.usescase.main.MainViewModel
import com.cice.practica3b.usescase.main.favorites.FavoritesFragment
import com.cice.practica3b.usescase.main.profile.ProfileFragment
import com.cice.practica3b.usescase.main.today.TodayFragment
import com.cice.practica3b.usescase.main.nextdays.NextDaysFragment

open class MainGlobalFragment : Fragment() {

    var vM: MainViewModel? = null

    companion object {

        fun newinstance(section: Int, args: Bundle? = null): MainGlobalFragment {

            val fragment: MainGlobalFragment
            when (section) {
                0 -> fragment = TodayFragment()
                1 -> fragment = NextDaysFragment()
                2 -> fragment = FavoritesFragment()
                3 -> fragment = ProfileFragment()
                else -> fragment = MainGlobalFragment()
            }
            fragment.arguments = args
            return fragment
        }
    }
}