package com.cice.practica3b.usescase.main

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.cice.practica3b.R
import com.cice.practica3b.databinding.ActivityMainBinding
import com.cice.practica3b.extensions.log
import com.cice.practica3b.extensions.customToast
import com.cice.practica3b.extensions.hideKeyboard
import com.cice.practica3b.model.Cities
import com.cice.practica3b.usescase.main.globals.*
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : GlobalActivity() {

    private lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainViewModel
    lateinit var aa: String
    var cc = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        viewModel = MainViewModel.create(this)

        viewModel.secondFavorite.value = readPreferences(SECOND)
        viewModel.thirdFavorite.value = readPreferences(THIRD)
        viewModel.fourthFavorite.value = readPreferences(FOURTH)
        viewModel.fifthFavorite.value = readPreferences(FIFTH)

        val numero = intent.getIntExtra(GOBACK, 0)

        if (numero == 1) {
            writePreferences(INICIO, GOBACK)

            when {
                (viewModel.secondFavorite.value.isNullOrEmpty() || readPreferences("a") == "D") -> {
                    viewModel.secondFavorite.value = readPreferences(FIRSTOPTION)
                    writePreferences(SECOND, viewModel.secondFavorite.value.toString())
                    writePreferences("a", "A")
                }

                (viewModel.thirdFavorite.value.isNullOrEmpty() || readPreferences("a") == "A")
                -> {
                    viewModel.thirdFavorite.value = readPreferences(FIRSTOPTION)
                    writePreferences(THIRD, viewModel.thirdFavorite.value.toString())
                    writePreferences("a", "B")
                }

                (viewModel.fourthFavorite.value.isNullOrEmpty() || readPreferences("a") == "B")
                -> {
                    viewModel.fourthFavorite.value = readPreferences(FIRSTOPTION)
                    writePreferences(FOURTH, viewModel.fourthFavorite.value.toString())
                    writePreferences("a", "C")
                }

                (viewModel.fifthFavorite.value.isNullOrEmpty() || readPreferences("a") == "C")
                -> {
                    viewModel.fifthFavorite.value = readPreferences(FIRSTOPTION)
                    writePreferences(FIFTH, viewModel.fifthFavorite.value.toString())
                    writePreferences("a", "D")
                }

                else -> writePreferences("a", "D")
            }
        }

        if (readPreferences(INICIO) != INICIADO) {

            button_main.setOnClickListener {

                when {

                    (edit_text_main.text.toString().length < 3) -> {
                        customToast(
                            MINIMUN, this,
                            R.drawable.toast, Toast.LENGTH_LONG
                        )
                    }

                    (edit_text_main.text.toString().length >= 3) -> {

                        val stream = this.assets.open("citylist.json")
                        val size = stream.available()
                        val buffer = ByteArray(size)

                        stream.read(buffer)
                        stream.close()

                        val result = buffer.toString(Charsets.UTF_8)
                        val citiesResponse = Gson().fromJson(result, Array<Cities>::class.java)
                        val bb = edit_text_main.text.toString().toLowerCase()

                        for (i in 0..200000) {

                            aa = citiesResponse[i].name.toLowerCase()

                            if (aa == bb) {

                                cc = aa

                                viewModel.firstCity = edit_text_main.text.toString()

                                writePreferences(FIRSTOPTION, viewModel.firstCity)
                                writePreferences(INICIO, INICIADO)

                                viewGone()
                                initializeTab()
                            }
                        }

                        if (cc == "")  {

                            customToast(
                                COINCIDENCIE, this,
                                R.drawable.toast, Toast.LENGTH_LONG
                            )
                        }
                    }
                }
            }
        } else {

            loadPreferences()
        }
    }

    private fun loadPreferences() {

        viewModel.firstCity = readPreferences(FIRSTOPTION)!!

        if (viewModel.firstCity.isNotEmpty()) {

            viewModel.loadCurrentWeather(viewModel.firstCity)

            viewGone()

            initializeTab()
        }
    }

    private fun initializeTab() {

        val tabTitles = resources.getStringArray(R.array.tab_titles)

        binding.tabLayout.setupWithViewPager(binding.tabViewPager)
        binding.tabViewPager.adapter = MainTabPagerAdapter(tabTitles, supportFragmentManager)

        for (i in 0..tabTitles.lastIndex) {
            binding.tabLayout.getTabAt(i)?.text = tabTitles[i]
        }

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {

                when (tab?.position) {

                    3 -> {
                        writePreferences(NAME, viewModel.profileName.value.toString())
                        writePreferences(EMAIL, viewModel.profileEmail.value.toString())
                    }
                }
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

                when (tab?.position) {

                    0 -> viewModel.loadCurrentWeather(q = viewModel.firstCity)

                    1 -> viewModel.loadForecastWeather(q = viewModel.firstCity)


                    3 -> {
                        viewModel.profileName.value = readPreferences(NAME)
                        viewModel.profileEmail.value = readPreferences(EMAIL)
                    }
                }
                tab?.text.toString().log()
            }
        })
    }

    private fun viewGone() {

        viewModel.loadCurrentWeather(edit_text_main.text.toString())

        edit_text_main.visibility = View.GONE
        button_main.visibility = View.GONE
        main_list.visibility = View.GONE
        hideKeyboard(this, edit_text_main)
    }
}