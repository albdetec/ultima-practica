package com.cice.practica3b.usescase.main.nextdays

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cice.practica3b.R
import com.cice.practica3b.databinding.FragmentNextdayBinding
import com.cice.practica3b.model.Weather
import com.cice.practica3b.usescase.main.MainViewModel
import com.cice.practica3b.usescase.main.globals.MainGlobalFragment

class NextDaysFragment : MainGlobalFragment() {

    private lateinit var binding: FragmentNextdayBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_nextday, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.let {

            vM = ViewModelProviders.of(it).get(MainViewModel::class.java)

            vM?.foreCast!!.observe(viewLifecycleOwner,  Observer<List<Weather>> {

                    forecast ->

                if (forecast != null)

                    binding.recyclerForecast.adapter = NextDaysAdapter(forecast as List<Weather>)

            } )


            binding.lifecycleOwner = this
        }
    }
}