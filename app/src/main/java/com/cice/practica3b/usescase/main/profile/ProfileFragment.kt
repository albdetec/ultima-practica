package com.cice.practica3b.usescase.main.profile

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.cice.practica3b.R
import com.cice.practica3b.databinding.FragmentProfileBinding
import com.cice.practica3b.usescase.main.MainViewModel
import com.cice.practica3b.usescase.main.globals.MainGlobalFragment
import com.google.firebase.auth.FirebaseAuth

import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment: MainGlobalFragment() {

    private lateinit var mAuth: FirebaseAuth

    private lateinit var mcontext: Context
    private lateinit var binding: FragmentProfileBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mAuth = FirebaseAuth.getInstance()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val currentUser = mAuth.currentUser

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        mcontext = context

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {

            vM = ViewModelProviders.of(it).get(MainViewModel::class.java)

            binding.viewModel = vM
            binding.lifecycleOwner = this
        }

            btn_profile.setOnClickListener {

                val ft = fragmentManager!!.beginTransaction()
                val prev = fragmentManager!!.findFragmentByTag("dialog")
                if (prev != null) {
                    ft.remove(prev)
                }

                ft.addToBackStack(null)
                val dialogFragment = ProfileDialogFragment()
                dialogFragment.show(ft, "dialog")
            }
    }
}