package com.cice.practica3b.usescase.main.today


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.cice.practica3b.R
import com.cice.practica3b.databinding.FragmentTodayBinding
import com.cice.practica3b.usescase.main.MainViewModel
import com.cice.practica3b.usescase.main.globals.MainGlobalFragment

class TodayFragment : MainGlobalFragment() {

    private lateinit var binding: FragmentTodayBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_today, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.let {

            vM = ViewModelProviders.of(it).get(MainViewModel::class.java)

            binding.viewModel = vM
            binding.lifecycleOwner = this
        }
    }
}