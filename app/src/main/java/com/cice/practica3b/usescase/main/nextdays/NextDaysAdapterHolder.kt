package com.cice.practica3b.usescase.main.nextdays


import androidx.recyclerview.widget.RecyclerView
import androidx.databinding.library.baseAdapters.BR
import com.cice.practica3b.databinding.ItemForecastBinding
import com.cice.practica3b.model.Weather

class NextDaysAdapterHolder(val binding: ItemForecastBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(forecast: Weather) {
        binding.setVariable(BR.forecast, forecast)
        binding.executePendingBindings()
    }
}