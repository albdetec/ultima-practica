package com.cice.practica3b.usescase.main.globals

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity

abstract class GlobalActivity : AppCompatActivity() {

    lateinit var preferences: SharedPreferences

    fun readPreferences(key: String): String? {

        preferences = getSharedPreferences(PREFNAME, Context.MODE_PRIVATE)

        val value = preferences.getString(key, "")

        return value
    }

    fun writePreferences(key: String, value: String) {

        val editor = preferences.edit()

        editor.putString(key, value)
        editor.apply()
    }
}