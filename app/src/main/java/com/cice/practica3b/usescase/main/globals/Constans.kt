package com.cice.practica3b.usescase.main.globals


const val APIKEY = "db6c07e78a9c410587d431a8dfd61ec5"
const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
const val LANG_ES = "&lang=es"
const val LANG_FR = "&LANG=fr"

const val PREFNAME = "PrefName"
const val FIRSTOPTION = "firstoption"
const val INICIO = "inicio"
const val INICIADO = "iniciado"
const val GOBACK = "goback"

const val CLEAR = "despejado"
const val CLOUDY = "nubes"
const val SUNNY = "claro"
const val RAIN = "lluvia"

const val NAME = "name"
const val EMAIL = "email"

const val COINCIDENCIE = "No se encuentra coincidencia"
const val MINIMUN = "Mínimo tres letras"

const val SECOND = "second"
const val THIRD = "third"
const val FOURTH = "fourth"
const val FIFTH = "fifth"

const val ACTION_NAME = "action_name"