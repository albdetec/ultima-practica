package com.cice.practica3b.usescase.main


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.cice.practica3b.model.*
import com.cice.practica3b.usescase.main.globals.*
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

class MainViewModel : ViewModel() {

    lateinit var firstCity: String
    lateinit var language: String
    lateinit var units: String

    var listWeather: ArrayList<Weather> = ArrayList()
    var listX: ArrayList<X> = ArrayList()
    var descriptionn: MutableLiveData<String> = MutableLiveData()

    val profileName: MutableLiveData<String> = MutableLiveData()
    val profileEmail: MutableLiveData<String> = MutableLiveData()

    var weatherCurrent: MutableLiveData<CurrentWeather> = MutableLiveData()
    val weathercurrent: LiveData<CurrentWeather>
        get() = weatherCurrent

    var xCast: MutableLiveData<List<X>> = MutableLiveData()
    var foreCast: MutableLiveData<List<Weather>> = MutableLiveData()
    var mainCast: MutableLiveData<Main> = MutableLiveData()

    var weatherForecast: MutableLiveData<WeatherForecast> = MutableLiveData()
    val weatherforecast: LiveData<WeatherForecast>
        get() = weatherForecast

    val secondFavorite: MutableLiveData<String> = MutableLiveData()
    val thirdFavorite: MutableLiveData<String> = MutableLiveData()
    val fourthFavorite: MutableLiveData<String> = MutableLiveData()
    val fifthFavorite: MutableLiveData<String> = MutableLiveData()

    companion object {
        fun create(activity: MainActivity): MainViewModel {
            return ViewModelProviders.of(activity).get(MainViewModel::class.java)
        }
    }

    fun loadProfile(name: String, email: String) {

        profileName.value = name
        profileEmail.value = email
    }

    fun loadCurrentWeather(q: String) {

        when {
            (Locale.getDefault() == localeES) -> {
                language = LANG_ES
                units = "&units=metric"
            }
            (Locale.getDefault() == localeFR) -> {
                language = LANG_FR
                units = "&units=imperial"
            }
            else -> language = ""
        }

        doAsync {
            val url = BASE_URL + "weather?q=" + q + "&appid=" + APIKEY + language + units
            val response = URL(url).readText()

            uiThread {

                val currentResponse = Gson().fromJson(response, CurrentWeather::class.java)

                weatherCurrent.value = currentResponse
                descriptionn.value = currentResponse.weather.get(0).description
            }
        }
    }

    fun loadForecastWeather(q: String) {

        when {
            (Locale.getDefault() == localeES) -> {
                language = LANG_ES
            }
            (Locale.getDefault() == localeFR) -> {
                language = LANG_FR
            }
            else -> language = ""
        }

        doAsync {
            val url = BASE_URL + "forecast?q=" + q + "&appid=" + APIKEY + language
            val response = URL(url).readText()


            uiThread {

                val foreCastResponse = Gson().fromJson(response, WeatherForecast::class.java)

                weatherForecast.value = foreCastResponse

                val jObj = JSONObject(response)
                val jArr = jObj.getJSONArray("list")

                for (i in 0..foreCastResponse.list.lastIndex) {

                    val jOblist = jArr.getJSONObject(i)
                    val jArrWeather = jOblist.getJSONArray("weather")
                    val jWeather = jArrWeather.getJSONObject(0).toString()
                    val jMain = jOblist.getJSONObject("main").toString()
                    val jX = jOblist.toString()

                    val xResponse = Gson().fromJson(jX, X::class.java)
                    val weatherResponse = Gson().fromJson(jWeather, Weather::class.java)
                    val mainResponse = Gson().fromJson(jMain, Main::class.java)

                    weatherResponse.dt = xResponse.dt

                    listX.add(xResponse)
                    listWeather.add(weatherResponse)

                    mainCast.value = mainResponse
                }

                val finalX = listX.toList()
                val finalWeather = listWeather.toList()

                xCast.value = finalX
                foreCast.value = finalWeather
            }
        }
    }
}