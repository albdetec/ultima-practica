package com.cice.practica3b.usescase.main

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.cice.practica3b.usescase.main.globals.MainGlobalFragment

class MainTabPagerAdapter (private val items: Array<String>, fragmentManager: FragmentManager):
    FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): MainGlobalFragment {

        return MainGlobalFragment.newinstance(position)
    }

        override fun getCount(): Int = items.size
}