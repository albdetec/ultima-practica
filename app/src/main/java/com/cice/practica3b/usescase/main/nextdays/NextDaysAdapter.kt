package com.cice.practica3b.usescase.main.nextdays

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cice.practica3b.R
import com.cice.practica3b.databinding.ItemForecastBinding
import com.cice.practica3b.model.Weather

class NextDaysAdapter(val forecast: List<Weather>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return forecast.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: ItemForecastBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_forecast, parent, false
        )
        return NextDaysAdapterHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as NextDaysAdapterHolder).bind(forecast[position])
    }
}