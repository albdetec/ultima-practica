package com.cice.practica3b.usescase.main.profile

import android.content.ContentValues.TAG
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.cice.practica3b.R
import com.cice.practica3b.usescase.main.MainViewModel
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_dialog.*

class ProfileDialogFragment : DialogFragment() {

    private lateinit var mAuth: FirebaseAuth
    private lateinit var vM: MainViewModel
    lateinit var email: String
    lateinit var password: String

    lateinit var dialogContext: Context

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val rootView = inflater.inflate(R.layout.fragment_dialog, container)

            mAuth = FirebaseAuth.getInstance()

            return rootView
        }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        dialogContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {

           vM = ViewModelProviders.of(it).get(MainViewModel::class.java)

            btnLogin.setOnClickListener {

                if (text_email_dialog.text.toString().isNotEmpty()
                    || text_password_dialog.text.toString().isNotEmpty())  {

                loadUser()

                signIn(email, password)  }
            }

            btnCreate.setOnClickListener {

                if (text_email_dialog.text.toString().isNotEmpty()
                    || text_password_dialog.text.toString().isNotEmpty())  {

                loadUser()

                val user = FirebaseAuth.getInstance().currentUser
                user?.let { val userEmail = user.email

                    if (text_email_dialog.text.toString() == userEmail)  {

                        signIn(email, password)
                    }

                    else

                        createUser(email, password)}  }
            }
        }
    }

    fun signIn(email: String, password: String)  {

        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener{ task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "signInWithEmail:success")
                    val user = mAuth.currentUser
                    dismiss()
                } else {
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    Toast.makeText(getActivity(), "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }


    }

    fun createUser(email: String, password: String) {

        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "createUserWithEmail:success")
                    dismiss()
                } else {
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(getActivity(), "Add user failed. Check the fields",
                        Toast.LENGTH_SHORT).show()
                }
            }


    }

    fun loadUser()  {

        email = text_email_dialog.text.toString()
        password = text_password_dialog.text.toString()

        vM.loadProfile(text_name_dialog.text.toString(), text_email_dialog.text.toString())
    }
}
