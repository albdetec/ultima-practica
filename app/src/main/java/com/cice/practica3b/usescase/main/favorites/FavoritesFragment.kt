package com.cice.practica3b.usescase.main.favorites

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.cice.practica3b.databinding.FragmentFavoritesBinding
import com.cice.practica3b.usescase.main.MainViewModel
import kotlinx.android.synthetic.main.fragment_favorites.*
import android.content.Intent
import com.cice.practica3b.R
import com.cice.practica3b.usescase.main.MainActivity
import com.cice.practica3b.usescase.main.globals.*

class FavoritesFragment : MainGlobalFragment() {

    private lateinit var binding: FragmentFavoritesBinding
    lateinit var favoritesContext: Context

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites, container, false)

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        favoritesContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.let {

            vM = ViewModelProviders.of(it).get(MainViewModel::class.java)

            text_principal.setOnClickListener {

                val intent = Intent(activity, MainActivity::class.java)
                intent.putExtra(GOBACK, 1)
                activity!!.startActivity(intent)
            }
        }

        binding.viewModel = vM
        binding.lifecycleOwner = this
    }
}